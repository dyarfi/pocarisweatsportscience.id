/*global Sportscience, Backbone*/

Sportscience.Collections = Sportscience.Collections || {};

(function () {
  'use strict';

  Sportscience.Collections.DataTriger = Backbone.Collection.extend({

	initialize: function (url) {

		this.url = url;

		return this;
	}

  });

})();
