/*global Sportscience, $*/

var sesData;
// var host = window.location.hostname + ':3000';
var host = window.location.hostname + '/apipss';

var apiUrl = 'http://' + host + '/';
var siteUrl = 'http://' + host + '/';
// var socket = io.connect(apiUrl);
var scrollPage;
var direct;
var chatId;

var Sportscience = (function () {
  var api = {
    Models: {},
    Collections: {},
    Views: {},
    Routers: {},
    init: function () {
      Backbone.history.start();
    },
    changeContent: function(el) {
      this.content.empty().append(el);
      return this;
    }
  };

  var ViewsFactory = {
    Home: function() {
      if (sesData == undefined) {
        var url = apiUrl + 'content';

        this.HomeView = new Sportscience.Views.Home({
          collection: new api.Collections.DataTriger(url),
        });
      } else {
        this.HomeView = new Sportscience.Views.Home();
      }
    },

    Post: function (id)
    {
      var url = apiUrl + 'article/' + id;

      this.PostView = new Sportscience.Views.Post({
        collection: new api.Collections.DataTriger(url),
      });
    },

    Event: function (id)
    {
      var url = apiUrl + 'event/' + id;

      this.EventView = new Sportscience.Views.Event({
        collection: new api.Collections.DataTriger(url),
      });
    },

    Video: function (title, id)
    {
      var url = apiUrl + 'video/' + id;

      this.VideoView = new Sportscience.Views.Video({
        collection: new api.Collections.DataTriger(url),
      });
    },

    Campaign: function (title)
    {
      var url = apiUrl + 'campaign/' + title;

      this.CampaignView = new Sportscience.Views.Campaign({
        collection: new api.Collections.DataTriger(url),
      });
    },

    Campaign: function (title, subtitle)
    {
      if (subtitle == undefined) {
        var url = apiUrl + 'campaign/' + title;

        scrollPage = false;
      } else {
        var url = apiUrl + 'campaign/' + title + '/' + subtitle;

        scrollPage = true;
      }

      this.CampaignView = new Sportscience.Views.Campaign({
        collection: new api.Collections.DataTriger(url),
      });
    },

    PostList: function (type)
    {
      var url = apiUrl + 'content/' + type;

      this.VideView = new Sportscience.Views.PostList({
        collection: new api.Collections.DataTriger(url),
      });
    },

    // Registration: function ()
    // {
    //   this.RegistrationView = new Sportscience.Views.Registration();
    // },

    // SuccessRegistration: function ()
    // {
    //   this.RegistrationView = new Sportscience.Views.SuccessRegistration();
    // },

    // UserInformation: function (id)
    // {
    //   var url = apiUrl + 'userDetail/' + id;

    //   this.UserInformationView = new Sportscience.Views.UserInformation({
    //     collection: new api.Collections.DataTriger(url),
    //   });
    // },

    // Personalization: function (id) {
    //   var url = apiUrl + 'userDetail/' + id;

    //   this.UserInformationView = new Sportscience.Views.Personalization({
    //     collection: new api.Collections.DataTriger(url),
    //   });
    // },

    // Livechat: function ()
    // {
    //   var url = apiUrl + 'chat-info/';

    //   this.LivechatView = new Sportscience.Views.Livechat({
    //     collection: new api.Collections.DataTriger(url),
    //   });
    // },

    // ExpertChat: function ()
    // {
    //   var url = apiUrl + 'chat/';

    //   this.ExpertChatView = new Sportscience.Views.ExpertChat({
    //     collection: new api.Collections.DataTriger(url),
    //   });
    // },

    // ExpertChatUser: function (id)
    // {
    //   var url = apiUrl + 'chat/' + id;

    //   this.ExpertChatView = new Sportscience.Views.ExpertChat({
    //     collection: new api.Collections.DataTriger(url),
    //   });
    // },

    // UserChat: function ()
    // {
    //   var url = apiUrl + 'chat/';

    //   this.UserChatView = new Sportscience.Views.UserChat({
    //     collection: new api.Collections.DataTriger(url),
    //   });
    // },

    // Login: function ()
    // {
    //   var url = apiUrl + 'login';

    //   this.LoginView = new Sportscience.Views.Login({
    //     collection: new api.Collections.DataTriger(url),
    //   });
    // }
  };

  var Router = Backbone.Router.extend({
    routes: {
      '': 'home',
      'home': 'home',
      'article/(:title)/(:id)': 'post',
      'video/(:title)/(:id)': 'video',
      'campaign/(:title)': 'campaign',
      'campaign/(:title)/(:subtitle)': 'campaign',
      'event/(:title)/(:id)': 'event',
      'content/(:type)': 'postList',
      // 'registration': 'registration',
      // 'registration-success': 'successRegistration',
      // 'user-detail/(:id)': 'userInformation',
      // 'personalization/(:id)': 'personalization',
      // 'livechat': 'livechat',
      // 'expert-chat': 'expertChat',
      // 'expert-chat/(:id)': 'expertChatUser',
      // 'user-chat': 'userChat',
      // 'login': 'login',
      // 'login/(:redirect)': 'loginRedirect'
    },

    home: function () {
      $('.nav-item').removeClass('active');

      $('.footer').show();

      return ViewsFactory.Home();
    },

    post: function (title, id) {
      $('.nav-item').removeClass('active');

      return ViewsFactory.Post(id);
    },

    event: function (title, id) {
      $('.nav-item').removeClass('active');

      return ViewsFactory.Event(id);
    },

    video: function (title, id) {
      $('.nav-item').removeClass('active');
      
      return ViewsFactory.Video(title, id);
    },

    campaign: function (title, subtitle) {
      $('.nav-item').removeClass('active');

      return ViewsFactory.Campaign(title, subtitle);
    },

    postList: function (type) {
      $('.nav-item').removeClass('active');

      $('.nav-' + type).addClass('active');

      $('.footer').show();

      return ViewsFactory.PostList(type);
    },

    // login: function () {
    //   direct = false;
      
    //   $('.nav-item').removeClass('active');

    //   $('.footer').show();

    //   return ViewsFactory.Login();
    // },

    // loginRedirect: function (redirect) {
    //   direct = redirect;

    //   $('.nav-item').removeClass('active');

    //   $('.footer').show();

    //   return ViewsFactory.Login();
    // },

    // registration: function () {
    //   $('.nav-item').removeClass('active');
      
    //   return ViewsFactory.Registration();
    // },

    // successRegistration: function () {
    //   $('.nav-item').removeClass('active');
      
    //   return ViewsFactory.SuccessRegistration();
    // },

    // userInformation: function (id) {
    //   $('.nav-item').removeClass('active');
      
    //   return ViewsFactory.UserInformation(id);
    // },

    // personalization: function (id) {
    //   $('.nav-item').removeClass('active');
      
    //   return ViewsFactory.Personalization(id);
    // },

    // livechat: function () {
    //   $('.nav-item').removeClass('active');

    //   return ViewsFactory.Livechat();
    // },

    // expertChat: function () {
    //   chatId = false;
      
    //   $('.nav-item').removeClass('active');

    //   $('.footer').hide();

    //   return ViewsFactory.ExpertChat();
    // },

    // expertChatUser: function (id) {
    //   chatId = id;

    //   $('.nav-item').removeClass('active');

    //   $('.footer').hide();

    //   return ViewsFactory.ExpertChatUser(id);
    // },

    // userChat: function () {
    //   $('.nav-item').removeClass('active');

    //   $('.footer').hide();

    //   return ViewsFactory.UserChat();
    // }
  });

  api.router = new Router();

  return api;
})();

$(document).ready(function () {
  'use strict';

  loadingContent();

  $.ajaxSetup({
    crossDomain: true,
    xhrFields: {
        withCredentials: true
    }
  });

  Sportscience.init();

  $(document).on('click', '.open-modal', function () {

    if ($('.modal').hasClass('in')) {
      $('.modal').modal('hide');
    }

    var target = $(this).attr('data-target');

    $('' + target).modal();

    return false;
  });
});

var timeLoading;
function loadingContent() {
  'use strict';

  var paceProgress = $('.pace-progress').attr('data-progress-text');

  if (paceProgress == '100%') {

    clearTimeout(timeLoading);

    $('body').addClass('stay-visible');

  } else {
    timeLoading = setTimeout(function () {

      loadingContent();
    }, 200);
  }
}