
/*global Sportscience, $*/
var authData;
var sesData;
// var host = window.location.hostname + ':3000';
// var host = window.location.hostname + ':4711';
var host = window.location.hostname + '/apipss_dev';
//var host = window.location.hostname + "/apipss";

// var host = 'http://pocarisweatsportscience.id/apipss';

var apiUrl = 'http://' + host + '/';
var siteUrl = 'http://' + host + '/';
// var socket = io.connect(host);
// var socket = io.connect(window.location.hostname + ":4711");
// var socket = io.connect('http://pocarisweatsportscience.id' + ':4711');
var scrollPage;
var direct;
var chatId;
var eventTab;

var Sportscience = (function () {
  var api = {
    Models: {},
    Collections: {},
    Views: {},
    Routers: {},
    init: function () {
      ViewsFactory.Navigation();

      Backbone.history.start();
    },
    changeContent: function(el) {
      this.content.empty().append(el);
      return this;
    }
  };

  var ViewsFactory = {

    Navigation: function() {
      var url = apiUrl + 'navigation';

      this.NavigationView = new Sportscience.Views.Navigation({
        collection: new api.Collections.DataTriger(url),
      });
    },
    Home: function() {
      if (sesData == undefined) {
        var url = apiUrl + 'content';

        this.HomeView = new Sportscience.Views.Home({
          collection: new api.Collections.DataTriger(url),
        });
      } else {
        this.HomeView = new Sportscience.Views.Home();
      }
    },


    Profile: function() {
      if (sesData == undefined) {
        var url = apiUrl + 'content';

        this.ProfileView = new Sportscience.Views.Profile({
          collection: new api.Collections.DataTriger(url),
        });
      } else {
        this.ProfileView = new Sportscience.Views.Profile();
      }
    },


    Post: function (id)
    {
      var url = apiUrl + 'article/' + id;

      this.PostView = new Sportscience.Views.Post({
        collection: new api.Collections.DataTriger(url),
      });
    },

    Event: function (id)
    {
      var url = apiUrl + 'event/' + id;

      this.EventView = new Sportscience.Views.Event({
        collection: new api.Collections.DataTriger(url),
      });
    },

    Video: function (title, id)
    {
      var url = apiUrl + 'video/' + id;

      this.VideoView = new Sportscience.Views.Video({
        collection: new api.Collections.DataTriger(url),
      });
    },

    Campaign: function (title)
    {
      var url = apiUrl + 'campaign/' + title;

      this.CampaignView = new Sportscience.Views.Campaign({
        collection: new api.Collections.DataTriger(url),
      });
    },

    Campaign: function (title, subtitle)
    {
      if (subtitle == undefined) {
        var url = apiUrl + 'campaign/' + title;

        scrollPage = false;
      } else {
        var url = apiUrl + 'campaign/' + title + '/' + subtitle;

        scrollPage = true;
      }

      this.CampaignView = new Sportscience.Views.Campaign({
        collection: new api.Collections.DataTriger(url),
      });
    },

    PostList: function (type)
    {
      var url = apiUrl + 'content/' + type;

      this.VideView = new Sportscience.Views.PostList({
        collection: new api.Collections.DataTriger(url),
      });
    },
    Sweat: function ()
    {
      this.SweatView = new Sportscience.Views.Sweat();
    },
    Heart: function ()
    {
      this.HeartView = new Sportscience.Views.Heart();
    },
    Calories: function ()
    {
      this.CaloriesView = new Sportscience.Views.Calories();
    }
  };

  var Router = Backbone.Router.extend({
    routes: {
      '': 'home',
      'home': 'home',
      'profile': 'profile',
      'article/(:title)/(:id)': 'post',
      'video/(:title)/(:id)': 'video',
      'campaign/(:title)': 'campaign',
      'campaign/(:title)/(:subtitle)': 'campaign',
      'event/(:title)/(:id)': 'event',
      'event/(:title)/(:id)/(:tab)': 'eventTab',
      'content/(:type)': 'postList',
      'tools/sweatrate' : 'sweat',
      'tools/heartrate' : 'heart',
      'tools/dailycalories' : 'calories'

    },

    home: function () {
      $('.nav-item').removeClass('active');

      $('.footer').show().removeClass('hidden-xs');

      return ViewsFactory.Home();
    },

    profile: function () {
      $('.nav-item').removeClass('active');

      $('.footer').show().removeClass('hidden-xs');

      return ViewsFactory.Profile();
    },

    post: function (title, id) {
      $('.nav-item').removeClass('active');

      return ViewsFactory.Post(id);
    },

    event: function (title, id) {
      eventTab = false;

      $('.nav-item').removeClass('active');

      return ViewsFactory.Event(id);
    },

    eventTab: function (title, id, tab) {
      eventTab = tab;

      $('.nav-item').removeClass('active');

      return ViewsFactory.Event(id);
    },

    video: function (title, id) {
      $('.nav-item').removeClass('active');

      return ViewsFactory.Video(title, id);
    },

    campaign: function (title, subtitle) {
      $('.nav-item').removeClass('active');

      return ViewsFactory.Campaign(title, subtitle);
    },

    postList: function (type) {
      $('.nav-item').removeClass('active');

      $('.nav-' + type).addClass('active');

      $('.footer').show().removeClass('hidden-xs');

      return ViewsFactory.PostList(type);
    },
    sweat: function () {
      $('.nav-item').removeClass('active');
      return ViewsFactory.Sweat();
    },
    heart: function(){
      $('.nav-item').removeClass('active');
      return ViewsFactory.Heart();
    },
    calories: function(){
      $('.nav-item').removeClass('active');
      return ViewsFactory.Calories();
    },
    login: function () {
      direct = false;

      $('.offset-swap').removeClass('nav-open');

      $('.nav-item').removeClass('active');

      $('.footer').show().addClass('hidden-xs');

      return ViewsFactory.Login();
    },
    loginRedirect: function (redirect) {
      direct = redirect;

      $('.nav-item').removeClass('active');

      $('.footer').show().addClass('hidden-xs');

      return ViewsFactory.Login();
    },
    registration: function () {

      $('.offset-swap').removeClass('nav-open');

      $('.nav-item').removeClass('active');

      return ViewsFactory.Registration();
    },
    successRegistration: function () {
      $('.nav-item').removeClass('active');

      return ViewsFactory.SuccessRegistration();
    },
    userInformation: function (id) {
      $('.nav-item').removeClass('active');

      return ViewsFactory.UserInformation(id);
    },
    personalization: function (id) {
      $('.offset-swap').removeClass('nav-open');

      $('.nav-item').removeClass('active');

      return ViewsFactory.Personalization(id);
    },

    /*
    livechat: function () {
      $('.offset-swap').removeClass('nav-open');

      $('.nav-item').removeClass('active');

      $('.footer').addClass('hidden-xs');

      return ViewsFactory.Livechat();
    },

    expertChat: function () {
      chatId = false;

      $('.offset-swap').removeClass('nav-open');

      $('.nav-item').removeClass('active');

      $('.footer').hide();

      return ViewsFactory.ExpertChat();
    },

    expertChatUser: function (id) {
      chatId = id;

      $('.offset-swap').removeClass('nav-open');

      $('.nav-item').removeClass('active');

      $('.footer').hide();

      return ViewsFactory.ExpertChatUser(id);
    },

    userChat: function (id) {
      $('.offset-swap').removeClass('nav-open');

      $('.nav-item').removeClass('active');

      $('.footer').hide();

      return ViewsFactory.UserChat(id);
    }
    */
  });

  api.router = new Router();

  return api;
})();

$(document).ready(function () {
  'use strict';

  loadingContent();

  $.ajaxSetup({
    crossDomain: true,
    xhrFields: {
        withCredentials: true
    }
  });

  Sportscience.init();

  $(document).on('click', '.offset-toggle', function () {

    var offset = $('.offset-swap');

    if (offset.hasClass('nav-open')) {
      offset.removeClass('nav-open');
    } else {
      offset.addClass('nav-open');
    }

    $('#homepage-slider').slick('unslick');
    $('#homepage-slider').slick({
      dots: true,
      infinite: true,
      speed: 900,
      slidesToShow: 1,
      autoplay: true,
      autoplaySpeed: 3000,
      prevArrow: '',
      nextArrow: ''
    });

    return false;
  });

  $(document).on('click', '.open-modal', function () {

    if ($('.modal').hasClass('in')) {
      $('.modal').modal('hide');
    }

    var target = $(this).attr('data-target');

    $('' + target).modal();

    return false;
  });
});

var timeLoading;

function loadingContent() {
  'use strict';

  var paceProgress = $('.pace-progress').attr('data-progress-text');

  if (paceProgress == '100%') {

    clearTimeout(timeLoading);

    $('body').addClass('stay-visible');

  } else {
    timeLoading = setTimeout(function () {

      loadingContent();
    }, 200);
  }
}

function ValidURL(s) {
   var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
   return regexp.test(s);
}
