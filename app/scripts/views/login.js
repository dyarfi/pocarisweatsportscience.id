/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

(function () {
  'use strict';

  Sportscience.Views.Login = Backbone.View.extend({

    template: JST['app/scripts/templates/login.ejs'],

    el: '#wrap',

    events: {
      'submit #login-form': 'submitLogin'
    },

    initialize: function () {
      this.$el.unbind();

      this.render();
    },

    render: function () {
      this.$el.html(this.template());

      sectionHeight();

      function sectionHeight() {
        var navHeight = $('.navbar').outerHeight();
        var footerHeight = $('.footer').outerHeight();
        var beforeFullHeight = $('.block.full-height').prev().outerHeight();

        $('.block.full-height').css({'height': ($(window).height() - navHeight - footerHeight - beforeFullHeight) + 'px'});
      }

      var rtime;
      var timeout = false;
      var delta = 200;
      $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
          timeout = true;

          setTimeout(resizeend, delta);
        }
      });

      function resizeend() {
        if (new Date() - rtime < delta) {
          setTimeout(resizeend, delta);
        } else {
          timeout = false;

          sectionHeight();
        }               
      }      
    },

    submitLogin: function (e) {
      e.preventDefault();

      var self = $(e.currentTarget);
      var form = $('#login-form');
      var selfFunction = this;

      $('#loading-overlay').fadeIn();

      $.post(apiUrl + 'login', form.serialize(), function (ui) {

        if (ui.response == 'success') {

          if (direct == 'chat') {
            if (ui.expert) {
              window.location.href = '#expert-chat';
            } else {
              window.location.href = '#user-chat/' + ui.id;
            }
          } else {
            window.location.href = '#home';
          }

          selfFunction.reinitNav();
        }

        $('#loading-overlay').fadeOut();
      });

      return false;
    },

    reinitNav: function () {
      var url = apiUrl + 'navigation';

      this.NavigationView = new Sportscience.Views.Navigation({
        collection: new Sportscience.Collections.DataTriger(url),
      });
    }

  });

})();
