/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

(function () {
  'use strict';

  Sportscience.Views.Home = Backbone.View.extend({

    template: JST['app/scripts/templates/home.ejs'],

    el: '#wrap',

    events: {},

    initialize: function () {
      this.$el.unbind();

      if (sesData == undefined) {
        this.collection.fetch();

        this.listenTo(this.collection, 'add', this.render);
      } else {
        this.render();
      }
    },

    render: function (item) {
      var self = this;

      if (sesData == undefined) {
        sesData = item.toJSON();
      }

      var html = this.$el.html(this.template(sesData));

      var slickElement = new Sportscience.Views.SlickElement();

      slickElement.render(html);

      $('#homepage-slider').slick({
        dots: true,
        infinite: true,
        speed: 900,
        slidesToShow: 1,
        prevArrow: '',
        nextArrow: '',
        autoplay: true,
        autoplaySpeed: 3000
      });
    }

  });

})();
