/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

(function () {
  'use strict';

  Sportscience.Views.Campaign = Backbone.View.extend({

    template: JST['app/scripts/templates/campaign.ejs'],

    el: '#wrap',

    events: {},

    initialize: function () {
      this.unbind();
      
      this.collection.fetch();

      this.listenTo(this.collection, 'add', this.render);
    },

    render: function (item) {
      var self = this;
      var body = $('html, body');

      var html = this.$el.html(this.template(item.toJSON()));

      var slickElement = new Sportscience.Views.SlickElement();

      slickElement.render(html);

      if (scrollPage) {
        setTimeout(function () {
          var startBox = html.find('#start-block').offset();

          //setTimeout(function () {
          body.stop().animate({scrollTop: startBox.top}, '500', 'swing');
          //}, 300);
        }, 300);
      } else {
        body.stop().animate({scrollTop: 0}, '500', 'swing');
      }
    }

  });

})();
