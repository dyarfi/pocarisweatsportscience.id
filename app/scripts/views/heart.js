/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

(function () {
  'use strict';

  Sportscience.Views.Heart = Backbone.View.extend({

    template: JST['app/scripts/templates/heart.ejs'],

    el: '#wrap',
    events: {
        "click .pocari_submit.button": "onSubmit",
    },
    initialize: function () {
      this.render();
    },

    render: function () {
      this.$el.html(this.template());
      var body = $('html, body');

      sectionHeight();

      function sectionHeight() {
        var navHeight = $('.navbar').outerHeight();
        var footerHeight = $('.footer').outerHeight();
        var beforeFullHeight = $('.block.full-height').prev().outerHeight();
        $('.block.full-height').css({'height': ($(window).height() - navHeight - footerHeight - beforeFullHeight) + 'px'});
      }

      var rtime;
      var timeout = false;
      var delta = 200;
      
      $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
          timeout = true;

          setTimeout(resizeend, delta);
        }
      });

      function resizeend() {
        if (new Date() - rtime < delta) {
          setTimeout(resizeend, delta);
        } else {
          timeout = false;

          sectionHeight();
        }               
      }      
      body.stop().animate({scrollTop: 0}, '500', 'swing', function () {
      });
      return this;
    },
    onSubmit: function(event) {
      event.preventDefault();
      var gender = this.$('#gender').val(),
      day_birth = this.$('#day_birth').val(),
      month_birth = this.$('#month_birth').val(),
      year_birth = this.$('#year_birth').val();
      var current_age = getAge(year_birth,month_birth,day_birth);
      function getAge(dateString) {
          var today = new Date();
          var birthDate = new Date(dateString);
          var age = today.getFullYear() - birthDate.getFullYear();
          var m = today.getMonth() - birthDate.getMonth();
          if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
              age--;
          }
          return age;
      }
      var heartrate_count = 220 - current_age;
      if(isNaN(heartrate_count)){
        var alert = "Data kurang untuk kalkulasi. Mohon isi semua kolom yang diperlukan."
        $('span.danger.form').text(alert);
        $('#show_heartrate_answer').hide('slow');
        return false;
      } else {
        $('span.danger.form').text('');
      }
      $('#age_age').text(current_age);
      $('#heartrate_').text(heartrate_count.toFixed(2));
      $('#show_heartrate_answer').show('slow');
      // $("#trigger_modal").modal();
    }
  });

})(jQuery);