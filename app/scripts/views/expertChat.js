/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

(function () {
  'use strict';

  var data;
  var timer;

  Sportscience.Views.ExpertChat = Backbone.View.extend({

    template: JST['app/scripts/templates/expertChat.ejs'],

    el: '#wrap',

    events: {
      'submit #messsage-form': 'submitMessage',
      'click .set-control': 'setControlTemplate',
      'click .save-as-template': 'saveAsTemplate',
      'click a[data-action="remove-template"]': 'removeTemplate'
    },

    initialize: function () {
      this.$el.unbind();

      this.collection.fetch();

      this.listenTo(this.collection, 'add', this.render);
    },

    render: function (item) {
      var self = this;

      data = item.toJSON();

      if (data.unauthorized) {

        window.location.href = '#login/chat';
        
      } else {
        this.$el.html(this.template(data));

        var chat = $('#chat');

        socket.on('message-notif', function (res) {
          if (res != chatId) {
            var userCol = $('a[data-id="'+res+'"]');
            var notif = userCol.find('.unread');
            var newCount = Number(notif.html()) + Number(1);

            if (newCount > 0) {
              notif.removeClass('hide');

              userCol.addClass('notif');
              setTimeout(function () {
                userCol.removeClass('notif');
              }, 2000);
            } else {
              notif.addClass('hide');
            }

            notif.html(newCount);
          }
        });

        socket.on('new message ' + chatId, function (res) {
          var position = ''

          if (data.main.expert != res.sender) {
            position = 'right';
          }

          if (res.sender == data.main.user.id) {
            position += ' you';
          }

          var html = 
          '<div class="messsage '+position+'" data-name="'+res.sender+'">'+
            '<div class="text">'+
              '<h4><span class="time">'+res.time+'</span> .'+res.name+'</h4>'+
              '<p>' + res.msg + '</p>'+
            '</div>'+
            '<div class="avatar" style="background-image: url(' + apiUrl + 'uploads/' + res.avatar + ');"></div>'+
          '</div>';

          var lastMessage = chat.find('.messsage').last();

          if (lastMessage.attr('data-name') == res.sender) {
            lastMessage.find('.text').append('<p>'+res.msg+'</p>')
          } else {
            chat.append(html);
          }

          chat.stop().animate({scrollTop: chat.prop('scrollHeight')}, '500', 'swing');
        });

        setTimeout(function () {
          socket.emit('login', data.main.user);
        }, 500);

        sectionHeight();

        function sectionHeight() {
          var navHeight = $('.navbar').outerHeight();
          var footerHeight = $('.footer').outerHeight();
          var beforeFullHeight = $('.block.full-height').prev().outerHeight();

          $('.block.full-height').css({'height': ($(window).height() - navHeight - beforeFullHeight) + 'px'});
        }

        setTimeout(function () {
          socket.on('get users', function (users) {
            var html = '';

            var userNumber = 0;

            for (var i = 0; i < users.length; i++) {

              if (data.main.user.id != users[i].id) {
                html += 
                '<a href="#expert-chat/'+users[i].id+'" data-id="'+users[i].id+'" class="item">'+users[i].full_name+
                  '<div class="toolbar text-right">'+
                    // '<div class="icon hide-icon ellipse"></div>'+
                    // '<div class="icon hide-icon remove"></div>'+
                    '<div class="icon unread hide">0</div>'+
                  '</div>'+
                '</a>';

                userNumber++;
              }
            }

            $('.online-user-count').html(userNumber);

            // if ($('#user').html() != '') {
            //   $('#user').prepend(html);
            // } else {
              // $('#user').html(html);
            // }
            
            $('#user').html(html);
          });
        }, 1000);

        timer = setInterval(function() {
          self.countDown(data.main.time_end);
        }, 1000);

        var rtime;
        var timeout = false;
        var delta = 200;
        $(window).resize(function() {
          rtime = new Date();
          if (timeout === false) {
            timeout = true;

            setTimeout(resizeend, delta);
          }
        });

        function resizeend() {
          if (new Date() - rtime < delta) {
            setTimeout(resizeend, delta);
          } else {
            timeout = false;

            sectionHeight();
          }               
        }      
      }
    },

    setControlTemplate: function (e) {
      e.preventDefault();

      var self = $(e.currentTarget);
      var id = self.attr('data-number');

      if (self.attr('data-action') == 'use-template') {
        var message = $('#template-' + id).html();

        $('textarea[name="message"]').val(message);
      }

      return false;
    },

    submitMessage: function (e) {
      e.preventDefault();

      var form = $(e.currentTarget);

      var message = form.find('textarea[name="message"]');

      var chat = {};

      chat.id = chatId;
      chat.sender = data.main.user.id;
      chat.name = data.main.user.full_name;
      chat.message = message.val();
      chat.expert = data.main.expert;

      if (data.main.user.avatar) {
        chat.avatar = data.main.user.avatar;
      } else {
        chat.avatar = '';
      }

      socket.emit('send message', chat);

      message.val('');

      return false;
    },

    saveAsTemplate: function (e) {
      e.preventDefault();

      var message = $('textarea[name="message"]');
      var listTemplate = $('#list-template');

      $.post(apiUrl + 'chat/saveAsTemplate', 'message=' + message.val(), function (ui) {
        if (ui.response == 'success') {
          var number = Number(listTemplate.find('.item').size()) + Number(1);

          var template = 
                  '<div class="item" id="template-'+ui.id+'">'+
                    '<p id="template-'+number+'">'+message.val()+'</p>'+
                    '<div class="control">'+
                      '<div class="row">'+
                        '<a href="#" class="columns col-xs-4 set-control" data-action="use-template" data-number="'+number+'"><i class="fa fa-upload"></i></a>'+
                        '<a href="#" class="columns col-xs-4 set-control" data-action="edit-template" data-number="'+number+'"><i class="fa fa-edit"></i></a>'+
                        '<a href="#" class="columns col-xs-4 set-control" data-action="remove-template" data-id="'+ui.id+'"><i class="fa fa-trash-o"></i></a>'+
                      '</div>'+
                    '</div>'+
                  '</div>';

          listTemplate.prepend(template);
        }
      }, 'json');

      return false;
    },

    removeTemplate: function (e) {
      e.preventDefault();

      var id = $(e.currentTarget).attr('data-id');

      $.post(apiUrl + 'chat/removeTemplate/' + id, '', function (ui) {
        if (ui.response == 'success') {
          $('#template-' + id).remove();
        }
      }, 'json');

      return false;
    },

    countDown: function (date) {
      var end = new Date(date);

      var _second = 1000;
      var _minute = _second * 60;
      var _hour = _minute * 60;
      var _day = _hour * 24;

      var now = new Date();
      var distance = end - now;

      var days = Math.floor(distance / _day);
      var hours = Math.floor((distance % _day) / _hour);
      var minutes = Math.floor((distance % _hour) / _minute);
      var seconds = Math.floor((distance % _minute) / _second);

      if (distance < 0) {

          clearInterval(timer);

          days = 0;
          hours = 0;
          minutes = 0;
          seconds = 0;
      }

      var countdown = '';
      countdown += '<span>' + pad(hours) + '</span>:';
      countdown += '<span>' + pad(minutes) + '</span>:';
      countdown += '<span>' + pad(seconds) + '</span>';

      $('#countdown').html(countdown);

      function pad(n) {
          return (n < 10) ? ("0" + n) : n;
      }
    }
  });

})();
