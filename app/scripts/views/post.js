/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

(function () {
  'use strict';

  Sportscience.Views.Post = Backbone.View.extend({

    template: JST['app/scripts/templates/post.ejs'],

    el: '#wrap',

    events: {},

    initialize: function () {
      this.$el.unbind();
      
      this.collection.fetch();

      this.listenTo(this.collection, 'add', this.render);
    },

    render: function (item) {
      var self = this;
      var body = $('html, body');

      var html = this.$el.html(this.template(item.toJSON()));

      var slickElement = new Sportscience.Views.SlickElement();

      slickElement.render(html);

      //body.lockscroll(true);

      body.stop().animate({scrollTop: 0}, '500', 'swing', function () {
        //body.lockscroll(false);
      });
    }

  });

})();
