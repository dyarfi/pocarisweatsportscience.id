/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

(function () {
  'use strict';

  Sportscience.Views.Profile = Backbone.View.extend({

    template: JST['app/scripts/templates/profile.ejs'],

    el: '#wrap',

    events: {},

    initialize: function () {
      this.$el.unbind();

      if (sesData == undefined) {
        this.collection.fetch();

        this.listenTo(this.collection, 'add', this.render);
      } else {
        this.render();
      }
    },

    render: function (item) {
      var self = this;

      if (sesData == undefined) {
        sesData = item.toJSON();
      }

      if (!authData.objectId) {
        window.location.href = '#login';

        reload();
      }

      sesData.user = authData;

      var html = this.$el.html(this.template(sesData));

      $('.offset-swap').addClass('nav-open');

      var slickElement = new Sportscience.Views.SlickElement();

      slickElement.render(html);
    }

  });

})();
