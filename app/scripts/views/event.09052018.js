/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

var timer;

(function () {
  'use strict';

  Sportscience.Views.Event = Backbone.View.extend({

    template: JST['app/scripts/templates/event.ejs'],

    el: '#wrap',

    events: {
      'click .panel-default': 'collapseToggle',
      'click .scrollto': 'scrollTo',
      'click li#anchor_type_matches' : 'scrollToLive'
    },

    initialize: function () {
      this.$el.unbind();

      this.collection.fetch();

      this.listenTo(this.collection, 'add', this.render);
    },

    render: function (item) {
      var self = this;
      var body = $('html, body');
      var data = item.toJSON();

      var html = this.$el.html(this.template(data));

      var slickElement = new Sportscience.Views.SlickElement();

      slickElement.render(html);
      //this.countDown();
      //body.lockscroll(true);

      //console.log(data.detail.content[0].event_date);
      //self.countDown(res);
      //var date = new Date(data.detail.date);
      var dated = (data.detail.content[0].countdown) ? data.detail.content[0].countdown.countdown_date : data.detail.content[0].date;
      var date = new Date(dated);
      var res = {
        'date' : date,
        'type' : 'hour'
      };
      self.countDown(res);
      //console.log(date.getTimezoneOffset());
      //console.log(res);

      body.stop().animate({scrollTop: 0}, '500', 'swing', function () {
        //body.lockscroll(false);
      });


      $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var active = $(e.target);

        if (active.attr('data-type') == 'c') {
          $('#related-block').hide();

          self.localEqualizer();
        } else if (active.attr('data-type') == 'a') {
          $('#related-block').show();
        } else if (active.attr('data-anchor') =='matches'){
          self.scrollToLive();
        }
        else {
          $('#related-block').hide();
        }
      });

      if (eventTab) {
        $('a[href="#'+eventTab+'"]').tab('show');
      }
    },

    scrollTo: function (e) {
      var self = $(e.currentTarget);
      var body = $('html, body');

      var destPost = $('' + self.attr('data-dest') + '').offset();

      body.stop().animate({scrollTop: destPost.top}, '1000', 'swing');

      return false;
    },
    scrollToLive: function(e){
      $('html,body').delay(1000).animate({
        scrollTop: $('#myDiv').offset().top
        }
        , 1500,'swing');
    },
    collapseToggle: function (e) {
      var self = $(e.currentTarget).find('a[data-toggle="collapse"]');
      var control = self.attr('aria-controls');
      var parent = self.attr('data-parent');
      var heading = self.attr('data-parent');


        $('' + parent + '').find('.panel-heading').removeClass('active');

      if ($('#' + control).hasClass('in')) {
        $('' + parent + '').find('.panel-collapse').removeClass('in');
      } else {
        $('' + parent + '').find('.panel-collapse').removeClass('in');

        $('#' + control).addClass('in');

        var heading = $('#' + control).attr('aria-labelledby');

        $('#' + heading).addClass('active');
      }

      return false;
    },

    localEqualizer: function () {

      var equalizer = $('.equalizer');

      var heightList = [];
      equalizer.find('.eq-item').each(function (i, value) {

        var iSelf = $(this);

        setTimeout(function () {
          if (iSelf.find('.same-height').size() > 0) {

            var heightPixel = iSelf.find('.same-height').outerHeight();

            heightList.push(heightPixel);
          }
        }, 400);
      });

      setTimeout(function () {
        var maxHeight = Math.max.apply(null, heightList);

        equalizer.find('.eq-item .same-height').css({'height': maxHeight + 'px'});
      }, 400);
    },

    countDown: function (res) {
      var end = new Date(res.date);

      var _second = 1000;
      var _minute = _second * 60;
      var _hour = _minute * 60;
      var _day = _hour * 24;

      var now = new Date();
      var distance = end - now;

      var days = Math.floor(distance / _day);
      var hours = Math.floor((distance % _day) / _hour);
      var minutes = Math.floor((distance % _hour) / _minute);
      var seconds = Math.floor((distance % _minute) / _second);
      /*
      if (distance < 0) {

          clearInterval(timer);

          days = 0;
          hours = 0;
          minutes = 0;
          seconds = 0;
      }

      var countdown = '';
      if (days > 0) {
        countdown += '<div class="item">' + pad(days) + '<span>DAYS</span></div><div class="separator">:</div>';
      }

      if (res.type == 'hour') {
        countdown += '<div class="item">' + pad(hours) + '<span>HOURS</span></div><div class="separator">:</div>';
        countdown += '<div class="item">' + pad(minutes) + '<span>MINUTES</span></div><div class="separator">:</div>';
        countdown += '<div class="item">' + pad(seconds) + '<span>SECONDS</span></div>';
      } else if (res.type == 'day') {
        countdown = '<div class="item">' + pad(days) + ' DAYS</div>';
      } else if (res.type == 'static_time') {
        countdown = '<div class="item">' + res.static_time + '</div>';

        clearInterval(timer);

        days = 0;
        hours = 0;
        minutes = 0;
        seconds = 0;
      }
      */

      if (res.type == 'hour') {
        $('#countdown').countdown(end, function(event) {

          var $this = $(this).html(event.strftime(''
          + '<div class="item time days"><div class="bg-item">%D</div><span class="label">DAYS</span></div><div class="separator">:</div>'
          + '<div class="item time hours"><div class="bg-item">%H</div><span class="label">HOURS</span></div><div class="separator">:</div>'
          + '<div class="item time minutes"><div class="bg-item">%M</div><span class="label">MINUTES</span></div><div class="separator">:</div>'
          + '<div class="item time seconds"><div class="bg-item">%S</div><span class="label">SECONDS</span></div>'));

        }).on('update.countdown', function(event) {

          //console.log(event.elapsed);
          //console.log($(this).find('.bg-item').fadeOut());

        });
      } else if (res.type == 'day') {

      } else if (res.type == 'static_time') {
        countdown = '<div class="item">' + res.static_time + '</div>';

        clearInterval(timer);

        days = 0;
        hours = 0;
        minutes = 0;
        seconds = 0;
      }

      // function pad(n) {
          //return (n < 10) ? ("0" + n) : n;
      //}

    }
  });

})();

function getDate (date) {

  if (date != '') {
    var date = new Date(date);

    return date.getDate();
  } else {
    return '';
  }
}

function getMonth (date) {

  if (date != '') {
    var date = new Date(date);

    var monthNames = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'];

    return monthNames[date.getMonth()];
  } else {
    return '';
  }
}
