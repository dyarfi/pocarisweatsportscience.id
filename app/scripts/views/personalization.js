/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

(function () {
  'use strict';

  Sportscience.Views.Personalization = Backbone.View.extend({

    template: JST['app/scripts/templates/personalization.ejs'],

    el: '#wrap',

    events: {
      'click .submit-data': 'submitData',
    },

    initialize: function () {
      this.$el.unbind();
      
      this.collection.fetch();

      this.listenTo(this.collection, 'add', this.render);
    },

    render: function (item) {
      var data = item.toJSON();

      var html = this.$el.html(this.template(data));

      sectionHeight();

      function sectionHeight() {
        var navHeight = $('.navbar').outerHeight();
        var footerHeight = $('.footer').outerHeight();
        var beforeFullHeight = $('.block.full-height').prev().outerHeight();

        $('.block.full-height').css({'height': ($(window).height() - navHeight - footerHeight - beforeFullHeight) + 'px'});
      }

      var rtime;
      var timeout = false;
      var delta = 200;
      $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
          timeout = true;

          setTimeout(resizeend, delta);
        }
      });

      function resizeend() {
        if (new Date() - rtime < delta) {
          setTimeout(resizeend, delta);
        } else {
          timeout = false;

          sectionHeight();
        }               
      }
    },

    submitData: function (event) {
      var self = $(event.currentTarget);
      var form = $('#data-form');

      $('#loading-overlay').fadeIn();

      $.post(apiUrl + 'auth/personalization', form.serialize(), function (ui) {

        if (ui.response == 'errorInput') {
          $.each(ui.message, function (i, value) {
            var group = $('#input-' + value.param);
            group.addClass('error');
            group.find('.message').html(value.msg);
          });
        } else if (ui.response == 'error') {
          console.log('error server');
        } else if (ui.response == 'success') {
          window.location.href = '#registration-success';
        }

        $('#loading-overlay').fadeOut();
      });

      return false;
    }
  });

})();
