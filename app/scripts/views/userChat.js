/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

(function () {
  'use strict';

  var data;
  var timer;

  Sportscience.Views.UserChat = Backbone.View.extend({

    template: JST['app/scripts/templates/userChat.ejs'],

    el: '#wrap',

    events: {
      'submit #messsage-form': 'submitMessage'
    },

    initialize: function () {
      this.$el.unbind();

      this.collection.fetch();

      this.listenTo(this.collection, 'add', this.render);
    },

    render: function (item) {
      data = item.toJSON();
      var self = this;

      if (data.unauthorized) {

        window.location.href = '#login/chat';
        
      } else {
        this.$el.html(this.template(data));

        var chat = $('#chat');

        socket.emit('login', data.main.user);

        socket.on('new message ' + data.main.user.id, function (res) {
          var position = ''

          if (data.main.expert != res.sender) {
            position = 'right';
          }

          if (res.sender == data.main.user.id) {
            position += ' you';
          }

          var html = 
          '<div class="messsage '+position+'" data-name="'+res.sender+'">'+
            '<div class="text">'+
              '<h4><span class="time">'+res.time+'</span> .'+res.name+'</h4>'+
              '<p>' + res.msg + '</p>'+
            '</div>'+
            '<div class="avatar" style="background-image: url(' + apiUrl + 'uploads/' + res.avatar + ');"></div>'+
          '</div>';

          var lastMessage = chat.find('.messsage').last();

          if (lastMessage.attr('data-name') == res.sender) {
            lastMessage.find('.text').append('<p>'+res.msg+'</p>')
          } else {
            chat.append(html);
          }

          chat.stop().animate({scrollTop: chat.prop('scrollHeight')}, '500', 'swing');
        });

        sectionHeight();

        function sectionHeight() {
          var navHeight = $('.navbar').outerHeight();
          var footerHeight = $('.footer').outerHeight();
          var beforeFullHeight = $('.block.full-height').prev().outerHeight();

          $('.block.full-height').css({'height': ($(window).height() - navHeight - beforeFullHeight) + 'px'});
        }

        setTimeout(function () {
          var userNumber = 0;

          socket.on('get users', function (users) {

            for (var i = 0; i < users.length; i++) {
              if (data.main.expert != users[i].id) {
                userNumber++;
              }
            }

            $('.online-user-count').html(userNumber);
          });
        }, 1000);

        timer = setInterval(function() {
          self.countDown(data.main.time_end);
        }, 1000);

        var rtime;
        var timeout = false;
        var delta = 200;
        $(window).resize(function() {
          rtime = new Date();
          if (timeout === false) {
            timeout = true;

            setTimeout(resizeend, delta);
          }
        });

        function resizeend() {
          if (new Date() - rtime < delta) {
            setTimeout(resizeend, delta);
          } else {
            timeout = false;

            sectionHeight();
          }               
        }      
      }
    },

    submitMessage: function (e) {
      e.preventDefault();

      var form = $(e.currentTarget);

      var message = form.find('textarea[name="message"]');

      var chat = {};

      chat.id = data.main.user.id;
      chat.sender = data.main.user.id;
      chat.name = data.main.user.full_name;
      chat.message = message.val();
      chat.expert = data.main.expert;

      if (data.main.user.avatar) {
        chat.avatar = data.main.user.avatar;
      } else {
        chat.avatar = '';
      }

      socket.emit('send message', chat);

      message.val('');

      return false;
    },

    countDown: function (date) {
      var end = new Date(date);

      var _second = 1000;
      var _minute = _second * 60;
      var _hour = _minute * 60;
      var _day = _hour * 24;

      var now = new Date();
      var distance = end - now;

      var days = Math.floor(distance / _day);
      var hours = Math.floor((distance % _day) / _hour);
      var minutes = Math.floor((distance % _hour) / _minute);
      var seconds = Math.floor((distance % _minute) / _second);

      if (distance < 0) {

          clearInterval(timer);

          days = 0;
          hours = 0;
          minutes = 0;
          seconds = 0;
      }

      var countdown = '';
      countdown += '<span>' + pad(hours) + '</span>:';
      countdown += '<span>' + pad(minutes) + '</span>:';
      countdown += '<span>' + pad(seconds) + '</span>';

      $('#countdown-userchat .stream-item').html(countdown);

      function pad(n) {
          return (n < 10) ? ("0" + n) : n;
      }
    }
  });

})();
