/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

(function () {
  'use strict';

  Sportscience.Views.Sweat = Backbone.View.extend({

    template: JST['app/scripts/templates/sweat.ejs'],

    el: '#wrap',
    events: {
        "click .pocari_submit.button": "onSubmit",
    },
    initialize: function () {
      this.render();
      $("#rangeslider_2").rangeslider();
      $("#rangeslider_3").rangeslider();

    },

    render: function () {
      this.$el.html(this.template());
      var body = $('html, body');

      sectionHeight();
    
      function sectionHeight() {
        var navHeight = $('.navbar').outerHeight();
        var footerHeight = $('.footer').outerHeight();
        var beforeFullHeight = $('.block.full-height').prev().outerHeight();
        $('.block.full-height').css({'height': ($(window).height() - navHeight - footerHeight - beforeFullHeight) + 'px'});
      }

      var rtime;
      var timeout = false;
      var delta = 200;
      
      $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
          timeout = true;

          setTimeout(resizeend, delta);
        }
      });

      function resizeend() {
        if (new Date() - rtime < delta) {
          setTimeout(resizeend, delta);
        } else {
          timeout = false;

          sectionHeight();
        }               
      }      
      body.stop().animate({scrollTop: 0}, '500', 'swing', function () {
      });
      return this;
    },
    onSubmit: function(event) {
      event.preventDefault();
      var before_weight = 40 + parseInt(this.$('#rangeslider_2').val());
      var after_weight = 40 + parseInt(this.$('#rangeslider_3').val());
      var liquid_consumed = parseInt(this.$('#liquid_consume').val());
      var exercise_time = parseInt(this.$('#hours_exercise').val());
      var cc = (before_weight - after_weight) * 1000;
      var dd = cc + liquid_consumed;
      var sweat_rate = dd / exercise_time;
      if(isNaN(sweat_rate)){
        var alert = "Data kurang untuk kalkulasi. Mohon isi semua kolom yang diperlukan."
        $('span.danger.form').text(alert);
        $('#show_answer_sweat').hide('slow');
        return false;
      } else {
        $('span.danger.form').text('');
      }    
        $('#weight_before_exercise').text(before_weight);
        $('#weight_after_exercise').text(after_weight);
        $('#liquid_consumed_').text(liquid_consumed);
        $('#exercise_time_').text(exercise_time);
        $('#show_answer_sweat').show('slow');
        $('#sweat_rate').text(sweat_rate.toFixed(2));
        // $("#trigger_modal").modal();
    }

  });

})(jQuery);