// /*global Sportscience, Backbone, JST*/

// Sportscience.Views = Sportscience.Views || {};

// (function () {
//   'use strict';

//   Sportscience.Views.SuccessRegistration = Backbone.View.extend({

//     template: JST['app/scripts/templates/successRegistration.ejs'],

//     el: '#wrap',

//     initialize: function () {
//       this.render();
//     },

//     render: function () {
//       this.$el.html(this.template());

//       sectionHeight();

//       function sectionHeight() {
//         var navHeight = $('.navbar').outerHeight();
//         var footerHeight = $('.footer').outerHeight();
//         var beforeFullHeight = $('.block.full-height').prev().outerHeight();

//         $('.block.full-height').css({'height': ($(window).height() - navHeight - footerHeight - beforeFullHeight) + 'px'});
//       }

//       var rtime;
//       var timeout = false;
//       var delta = 200;
//       $(window).resize(function() {
//         rtime = new Date();
//         if (timeout === false) {
//           timeout = true;

//           setTimeout(resizeend, delta);
//         }
//       });

//       function resizeend() {
//         if (new Date() - rtime < delta) {
//           setTimeout(resizeend, delta);
//         } else {
//           timeout = false;

//           sectionHeight();
//         }               
//       }      
//     }

//   });

// })();
