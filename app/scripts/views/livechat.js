/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

(function () {
  'use strict';

  var data;
  var timer;

  Sportscience.Views.Livechat = Backbone.View.extend({

    template: JST['app/scripts/templates/livechat.ejs'],

    el: '#wrap',

    events: {
    },

    initialize: function () {
      this.$el.unbind();

      this.collection.fetch();

      this.listenTo(this.collection, 'add', this.render);
    },

    render: function (item) {
      var self = this;
      data = item.toJSON();

      this.$el.html(this.template(data));

      sectionHeight();

      function sectionHeight() {
        var navHeight = $('.navbar').outerHeight();
        var footerHeight = $('.footer').outerHeight();
        var beforeFullHeight = $('.block.full-height').prev().outerHeight();

        $('.block.full-height').css({'height': ($(window).height() - navHeight - footerHeight - beforeFullHeight) + 'px'});
      }
      
      setTimeout(function () {
        socket.on('get users', function (users) {
          var userNumber = 0;

          for (var i = 0; i < users.length; i++) {
            userNumber++;
          }

          $('.online-user-count').html(userNumber);
        });
      }, 1000);

      timer = setInterval(function() {
        self.countDown(data.main.time_end);
      }, 1000);

      var rtime;
      var timeout = false;
      var delta = 200;
      $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
          timeout = true;

          setTimeout(resizeend, delta);
        }
      });

      function resizeend() {
        if (new Date() - rtime < delta) {
          setTimeout(resizeend, delta);
        } else {
          timeout = false;

          sectionHeight();
        }               
      }      
    },

    countDown: function (date) {
      var end = new Date(date);

      var _second = 1000;
      var _minute = _second * 60;
      var _hour = _minute * 60;
      var _day = _hour * 24;

      var now = new Date();
      var distance = end - now;

      var days = Math.floor(distance / _day);
      var hours = Math.floor((distance % _day) / _hour);
      var minutes = Math.floor((distance % _hour) / _minute);
      var seconds = Math.floor((distance % _minute) / _second);

      if (distance < 0) {

          clearInterval(timer);

          days = 0;
          hours = 0;
          minutes = 0;
          seconds = 0;
      }

      var countdown = '';
      countdown += '<div class="item"><span>' + pad(hours) + '</span><small>Hours</small></div>';
      countdown += '<div class="item"><span>' + pad(minutes) + '</span><small>Minutes</small></div>';
      countdown += '<div class="item"><span>' + pad(seconds) + '</span><small>Seconds</small></div>';

      $('#countdown .stream-item').html(countdown);

      function pad(n) {
          return (n < 10) ? ("0" + n) : n;
      }
    }
  });

})();
