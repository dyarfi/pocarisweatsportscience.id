/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

(function () {
  'use strict';

  Sportscience.Views.UserInformation = Backbone.View.extend({

    template: JST['app/scripts/templates/userInformation.ejs'],

    el: '#wrap',

    events: {
      'click .submit-data': 'submitData',
      'click .browse-avatar': 'browserFile'
    },

    initialize: function () {
      this.$el.unbind();
      
      this.collection.fetch();

      this.listenTo(this.collection, 'add', this.render);
    },

    render: function (item) {
      var data = item.toJSON();

      var html = this.$el.html(this.template(data));

      sectionHeight();

      function sectionHeight() {
        var navHeight = $('.navbar').outerHeight();
        var footerHeight = $('.footer').outerHeight();
        var beforeFullHeight = $('.block.full-height').prev().outerHeight();

        $('.block.full-height').css({'height': ($(window).height() - navHeight - footerHeight - beforeFullHeight) + 'px'});
      }

      html.find('#userfile').change(function () {
          $('#upload-form').attr('action', apiUrl + 'profile/avatar').submit();
      });

      $('#upload-form').ajaxForm({
          dataType:  'json',
          beforeSend: function() {

            $('#loading-upload').fadeIn();

            $('#loading-upload').find('.progress-bar').html('0%');

            $('#loading-upload').find('.progress-bar').width('0%');
          },
          uploadProgress: function(event, position, total, percentComplete) {
            var pVel = percentComplete + '%';

            $('#loading-upload').find('.progress-bar').width(pVel);

            $('#loading-upload').find('.progress-bar').html(pVel);
          },
          complete: function(dataUpload) {

            $('#loading-upload').fadeOut();

            if (dataUpload.responseJSON.response == 'success') {

              $('.browse-avatar').css({'background-image': 'url(' + apiUrl + 'uploads/' + dataUpload.responseJSON.path + ')'}).addClass('filled');
            } else {
            }
          }

      });

      var rtime;
      var timeout = false;
      var delta = 200;
      $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
          timeout = true;

          setTimeout(resizeend, delta);
        }
      });

      function resizeend() {
        if (new Date() - rtime < delta) {
          setTimeout(resizeend, delta);
        } else {
          timeout = false;

          sectionHeight();
        }               
      }

      $('.offset-swap').removeClass('nav-open');
    },

    browserFile: function (event) {
      var self = $(event.currentTarget);

      $('#userfile').click();

      return false;
    },

    submitData: function (event) {
      var self = $(event.currentTarget);
      var form = $('#data-form');

      $('#loading-overlay').fadeIn();

      $.post(apiUrl + 'auth/userDetail', form.serialize(), function (ui) {

        if (ui.response == 'errorInput') {
          $.each(ui.message, function (i, value) {
            var group = $('#input-' + value.param);
            group.addClass('error');
            group.find('.message').html(value.msg);
          });
        } else if (ui.response == 'error') {
          console.log('error server');
        } else if (ui.response == 'success') {
          window.location.href = '#personalization/' + ui.objectId;
        }

        $('#loading-overlay').fadeOut();
      });

      return false;
    }
  });

})();
