/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

(function () {
  'use strict';

  Sportscience.Views.Registration = Backbone.View.extend({

    template: JST['app/scripts/templates/registration.ejs'],

    el: '#wrap',

    events: {
      'click .submit-regist': 'submitRegist'
    },

    initialize: function () {
      this.$el.unbind();

      this.render();
    },

    render: function () {
      this.$el.html(this.template());

      sectionHeight();

      function sectionHeight() {
        var navHeight = $('.navbar').outerHeight();
        var footerHeight = $('.footer').outerHeight();
        var beforeFullHeight = $('.block.full-height').prev().outerHeight();

        $('.block.full-height').css({'height': ($(window).height() - navHeight - footerHeight - beforeFullHeight) + 'px'});
      }

      var rtime;
      var timeout = false;
      var delta = 200;
      $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
          timeout = true;

          setTimeout(resizeend, delta);
        }
      });

      function resizeend() {
        if (new Date() - rtime < delta) {
          setTimeout(resizeend, delta);
        } else {
          timeout = false;

          sectionHeight();
        }               
      }
    },

    submitRegist: function (event) {
      var self = $(event.currentTarget);
      var form = $('#regist-form');

      $('#loading-overlay').fadeIn();

      $.post(apiUrl + 'auth/registration', form.serialize(), function (ui) {

        if (ui.response == 'errorInput') {
          $.each(ui.message, function (i, value) {
            var group = $('#input-' + value.param);
            group.addClass('error');
            group.find('.message').html(value.msg);
          });
        } else if (ui.response == 'error') {
          console.log('error server');
        } else {
          window.location.href = '#user-detail/' + ui.objectId;
        }

        $('#loading-overlay').fadeOut();
      }, "json");

      return false;
    }

  });

})();
