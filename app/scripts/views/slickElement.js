/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

(function () {
  'use strict';

  Sportscience.Views.SlickElement = Backbone.View.extend({

    el: '#wrap',

    render: function (html) {
      var self = this;

      setColSize();

      function setColSize()
      {
        html.find('.slide-horizontal').each(function (x, xvalue) {

          var xSelf = $(this);

          xSelf.attr('id', 'slide-' + x);

          var heightList = [];
          xSelf.find('.columns').each(function (i, value) {

            var iSelf = $(this);

            iSelf.removeAttr('style');

            iSelf.css({'width' : iSelf.outerWidth() + 'px'});

            setTimeout(function () {
              if (iSelf.find('.text').size() > 0) {
                iSelf.find('.text').removeAttr('style');

                var heightPixel = iSelf.find('.text').outerHeight();

                heightList.push(heightPixel);
              }
            }, 400);
          });

          setTimeout(function () {
            var maxHeight = Math.max.apply(null, heightList);

            xSelf.find('.content-thumb .text').css({'height': maxHeight + 'px'});
          }, 400);
        });
      }

      html.find('.slide-horizontal').each(function (i, value) {
        $('#slide-' + i).slick({
          slidesToShow: 4,
          dots: false,
          infinite: false,
          variableWidth: true,
          prevArrow: '<a href="#" class="left slick-control"><img src="images/arrow-left-lg.png"></a>',
          nextArrow: '<a href="#" class="right slick-control"><img src="images/arrow-right-lg.png"></a>',
          responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
              }
            }
          ]          
        });
      });

      var rtime;
      var timeout = false;
      var delta = 200;
      $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
          timeout = true;

          setTimeout(resizeend, delta);
        }
      });

      function resizeend() {
        if (new Date() - rtime < delta) {
          setTimeout(resizeend, delta);
        } else {
          timeout = false;

          html.find('.slide-horizontal').each(function (i, value) {
            $('#slide-' + i).slick('destroy');
          });
          
          self.render(html);

          //setColSize();
        }               
      }
    }

  });

})();
