/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

(function () {
  'use strict';

  Sportscience.Views.Calories = Backbone.View.extend({

    template: JST['app/scripts/templates/calories.ejs'],

    el: '#wrap',
    events: {
        "click .pocari_submit.button": "onSubmit",
    },
    initialize: function () {
      this.render();
      $("#rangeslider_2").rangeslider();
      $("#rangeslider_1").rangeslider();

    },

    render: function () {
      this.$el.html(this.template());
      var body = $('html, body');

      sectionHeight();

      function sectionHeight() {
        var navHeight = $('.navbar').outerHeight();
        var footerHeight = $('.footer').outerHeight();
        var beforeFullHeight = $('.block.full-height').prev().outerHeight();
        $('.block.full-height').css({'height': ($(window).height() - navHeight - footerHeight - beforeFullHeight) + 'px'});
      }

      var rtime;
      var timeout = false;
      var delta = 200;
      
      $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
          timeout = true;

          setTimeout(resizeend, delta);
        }
      });

      function resizeend() {
        if (new Date() - rtime < delta) {
          setTimeout(resizeend, delta);
        } else {
          timeout = false;

          sectionHeight();
        }               
      }      
      body.stop().animate({scrollTop: 0}, '500', 'swing', function () {
      });
      return this;
    },
    onSubmit: function(event){
      event.preventDefault();
      var height = 100 + parseInt(this.$('#rangeslider_1').val());
      var weight = 40  + parseInt(this.$('#rangeslider_2').val());
      var gender = this.$('#gender').val(),
      day_birth = this.$('#day_birth').val(),
      month_birth = this.$('#month_birth').val(),
      year_birth = this.$('#year_birth').val();      

      var current_age = getAge(year_birth,month_birth,day_birth);
      
      // console.log(gender);
      // console.log(day_birth);
      // console.log(month_birth);
      // console.log(year_birth);
      // console.log(current_age);
      function getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
      }
      if(gender != ''){
        if(gender == 'male'){
          var age = parseInt(current_age);
          var weight_ = 9.6 * weight;
          var height_ = 1.8 * height;
          var age_ = 4.7 * age;
          var BMR = (655 + weight_ + height_) - age_;
        }
        if(gender == 'female'){
          var age = parseInt(current_age);
          var weight_ = 13.7 * weight;
          var height_ = 5 * height;
          var age_ = 6.8 * age;
          var BMR = (66 + weight_ + height_) - age_;
        }
      }
      if(isNaN(BMR && current_age)){
        var alert = "Data kurang untuk kalkulasi. Mohon isi semua kolom yang diperlukan."
        $('span.danger.form').text(alert);
        $('#show_calories_answer').hide('slow');
        return false;
      } else {
        $('span.danger.form').text('');
      }
      // console.log(BMR);
      $('#weight_text').text(weight);
      $('#height_text').text(height);
      $('#gender_text').text(gender);
      $('#age_text').text(current_age);
      $('#bmr_bmr').text(BMR.toFixed(2));
      $('#show_calories_answer').show('slow');
      // $("#trigger_modal").modal();
    }

  });

})(jQuery);