/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

(function () {
  'use strict';

  Sportscience.Views.Video = Backbone.View.extend({

    template: JST['app/scripts/templates/video.ejs'],

    el: '#wrap',

    events: {
      'click .see-more-video': 'seeMoreVideo'
    },

    initialize: function () {
      this.unbind();
      
      this.collection.fetch();

      this.listenTo(this.collection, 'add', this.render);
    },

    render: function (item) {
      var self = this;
      var body = $('html, body');

      var html = this.$el.html(this.template(item.toJSON()));

      var slickElement = new Sportscience.Views.SlickElement();

      slickElement.render(html);

      body.stop().animate({scrollTop: 0}, '500', 'swing');

      $(window).scroll(function(){
        var scrollPos = $(document).scrollTop();

        var vidOffset = $('#related-video').offset();
        var winHeight = $(window).height();

        var stopOffset = Number(winHeight) - Number(vidOffset.top);

        $('.bottom-fixed').show();

        if (scrollPos >= stopOffset) {
          //$('.bottom-fixed').hide();
        }
      });      
    },

    seeMoreVideo: function () {
      var body = $('html, body');
      var vidOffset = $('#related-video').offset();

      body.stop().animate({scrollTop: vidOffset.top}, '500', 'swing');

      return false;
    }

  });

})();
