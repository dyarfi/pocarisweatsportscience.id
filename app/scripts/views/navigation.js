/*global Sportscience, Backbone, JST*/

Sportscience.Views = Sportscience.Views || {};

(function () {
  'use strict';

  Sportscience.Views.Navigation = Backbone.View.extend({

    template: JST['app/scripts/templates/navigation.ejs'],

    el: '#navigation',

    events: {},

    initialize: function () {
      this.collection.fetch();

      this.listenTo(this.collection, 'add', this.render);
    },

    render: function (item) {
      var navData = item.toJSON();

      authData = navData.user;

      if (authData.objectId) {
        $('.auth-link').html('<a href="' + apiUrl + 'logout"><i class="fa fa-signout"></i> Logout</a>');
      } else {
        $('.auth-link').html('<a href="#login"><i class="fa fa-signin"></i> Login</a>');
      }

      this.$el.html(this.template(navData));
    }

  });

})();
