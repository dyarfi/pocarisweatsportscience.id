var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var NavigationSchema = new Schema({
	name: String,
	slug: String,
	order_number: String,
	status: String,
	type: String,
	based_on: String
});

mongoose.Promise = global.Promise;

mongoose.model('navigation', NavigationSchema, 'navigation');