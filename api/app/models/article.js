var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var ArticlesSchema = new Schema({
	header_type: String,
	type: String,
	campaign: Object,
	category: String,
	title: String,
	caption: String,
	content: Schema.Types.Mixed,
	image: String,
	image_thumbnail: String,
	image_background: String,
    writer: Array,
    status: String,
    date: Date,
    columns: String,
    tag: String,
    parent_id: String
});

mongoose.Promise = global.Promise;

mongoose.model('articles', ArticlesSchema, 'articles');