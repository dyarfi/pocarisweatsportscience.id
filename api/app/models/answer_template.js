var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var LivechatSchema = new Schema({
	text: String
});

mongoose.Promise = global.Promise;

mongoose.model('answer_template', LivechatSchema, 'answer_template');