var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var UserOnlineSchema = new Schema({
	uid: String,
	full_name: String,
	avatar: String,
	message: Array,
	unread: String,
	updated_date: Date
});

mongoose.Promise = global.Promise;

mongoose.model('user_online', UserOnlineSchema, 'user_online');