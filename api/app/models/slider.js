var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var SliderSchema = new Schema({
	image: String,
	image_mobile: String,
	order_number: String,
	url: String,
	status: String
});

mongoose.Promise = global.Promise;

mongoose.model('slider', SliderSchema, 'slider');