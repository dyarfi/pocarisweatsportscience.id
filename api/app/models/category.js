var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var CategoriesSchema = new Schema({
	name: String,
	alias: String
});

mongoose.Promise = global.Promise;

mongoose.model('categories', CategoriesSchema, 'categories');