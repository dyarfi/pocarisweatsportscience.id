var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var LivechatSchema = new Schema({
	topic: String,
	caption: String,
	expert: String,
	expert_name: String,
	expert_image: String,
	time_start: Date,
	date: String,
	time_end: Date,
	active_chat: String
});

mongoose.Promise = global.Promise;

mongoose.model('livechat', LivechatSchema, 'livechat');