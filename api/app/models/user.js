var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  bcrypt = require('bcrypt-nodejs');

var UserSchema = new Schema({
	facebook_id: String,
	full_name: String,
	email: String,
	password: String,
	gender: String,
	address: String,
	birthday: Date,
	avatar: String,
	sport: String,
	range: String,
    date: Date,
    status: String
});

UserSchema.pre('save', function (callback) {
	var user = this;

	if (!user.isModified('password'))
		return callback();

	bcrypt.genSalt(5, function (err, salt) {
		if (err)
			return callback(err);

		bcrypt.hash(user.password, salt, null, function (err, hash) {
			if (err)
				return callback(err);

			user.password = hash;

			callback();
		});
	});
});

UserSchema.methods.verifyPassword = function (password, callback) {
	bcrypt.compare(password, this.password, function (err, isMatch) {
		if (err)
			return callback(err);

		callback(null, isMatch);
	})
}

mongoose.Promise = global.Promise;

mongoose.model('User', UserSchema);