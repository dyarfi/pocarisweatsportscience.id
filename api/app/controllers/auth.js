var express = require('express'),
  Async = require('async'),
  path = require('path'),
  cookieParser = require('cookie-parser'),
  bodyParser = require('body-parser'),
  mongoose = require('mongoose'),
  passport = require('passport'),
  LocalStrategy = require('passport-local').Strategy,
  FacebookStrategy = require('passport-facebook').Strategy,
  findOrCreate = require('mongoose-findorcreate'),
  User = mongoose.model('User'),
  Livechat = mongoose.model('livechat');

var frontUrl = 'http://pocarisweatsportscience.id/dev';
var baseUrl = 'http://pocarisweatsportscience.id:4712';

module.exports = function (app) {

  app.use(passport.initialize());
  app.use(passport.session());

  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

  passport.use(new LocalStrategy({
      session: true
    },
    function(username, password, callback){
    User.findOne({email: username}, function (err, user) {
      if (err)
        return callback(err);

      if (!user)
        return callback(null, false, { message: 'Incorrect username.'});

      user.verifyPassword(password, function (err, isMatch) {
        if (err)
          return callback(err);

        if (!isMatch)
          return callback(null, false, { message: 'Incorrect password.'});

        return callback(null, user);
      });
    });
  }));

  passport.use(new FacebookStrategy({
      clientID: '333458700406545',
      clientSecret: '6eb0fe96983cf007aff6cf4c1ccc38ee',
      callbackURL: baseUrl + '/auth/facebook/callback',
      profileFields: ['id', 'displayName', 'photos', 'email', 'gender', 'birthday'],
      enableProof: true
    },
    function(accessToken, refreshToken, profile, callback) {

      console.log(profile);

      // return callback(null, profile);

      User.findOne({email: profile.emails[0].value}, function(err, user) {
        if (err)
          return callback(err);

        if (user) {
          return callback(null, user);
        } else {

          var newUser = new User();

          newUser.facebook_id = profile.id;
          newUser.full_name = profile.displayName;
          newUser.email = profile.emails[0].value;
          newUser.gender = profile.gender;
          newUser.avatar = 'https://graph.facebook.com/'+profile.id+'/picture?type=large';
          newUser.status = 0;
          newUser.date = new Date();

          newUser.save(function (err, data) {
            if (err)
              throw err;

            return callback(null, newUser);
          });
        }
      });
    }
  ));

  app.get('/auth/facebook', passport.authenticate('facebook', {scope: ['email','public_profile']}));

  /*
  app.get('/auth/facebook/callback',
    passport.authenticate('facebook', { failureRedirect: '/login' }),
    function(req, res) {
      res.redirect(frontUrl);
  });
  */

  app.get('/auth/facebook/callback', function(req, res, next) {
    var isExpert;

    passport.authenticate('facebook', { failureRedirect: '/login' }, function(err, user, info) {

      if (err)
        return next(err);

      if (!user)
        return res.send({unauthorized: true});

        Async.series([
          function (callback) {

            Livechat.findOne({expert: user._id}, function (err, chat) {
              if (err) res.send(err);

              isExpert = chat;

              if (chat == null) {
                isExpert = false;
              } else {
                isExpert = true;
              }

              callback();
            });
          }
        ], function (err) {
          if (err) return next(err);

          req.logIn(user, function (err) {
            if (err)
              return next(err);

            if (user.status == 0) {
              res.redirect(frontUrl + '#user-detail/' + user._id);
            } else {
              res.redirect(frontUrl + '#profile');
            }
          });
        });

    })(req, res, next);
  });  

  app.route('/login')
  .post(function (req, res, next) {

    var isExpert;

    passport.authenticate('local', { session: false }, function(err, user, info) {

      if (err)
        return next(err);

      if (!user)
        return res.send({unauthorized: true});

        Async.series([
          function (callback) {

            Livechat.findOne({expert: user._id}, function (err, chat) {
              if (err) res.send(err);

              isExpert = chat;

              if (chat == null) {
                isExpert = false;
              } else {
                isExpert = true;
              }

              callback();
            });
          }
        ], function (err) {
          if (err) return next(err);

          req.logIn(user, function (err) {
            if (err)
              return next(err);

            return res.send({response: 'success', expert: isExpert, id: user._id});
          });
        });

    })(req, res, next);
  });

  app.get('/logout', function(req, res) {
    req.logout();

    return res.redirect(frontUrl);
  });  
};