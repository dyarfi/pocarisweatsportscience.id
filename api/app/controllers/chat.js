var express = require('express'),
  passport = require('passport'),
  Async = require('async'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Livechat = mongoose.model('livechat'),
  AnswerTemplate = mongoose.model('answer_template'),
  UserOnline = mongoose.model('user_online');

users = [];

module.exports = function (app) {

  function getIndexBy(array, name, value) {

    for (var i = 0; i < array.length; i++) {
      if (array[i][name] == value) {
        return i;
      }
    }

    return -1;
  }

  function pad(n) {
    return (n < 10) ? ('0' + n) : n;
  }

  function getMid(n) {
    return (n > 12) ? (n + ' PM') : (n + ' AM');
  }

  app.route('/chat-info')
  .get(function (req, res, next) {

  	var contentChat = {};

	  Async.series([
	    function (callback) {

		    Livechat.findOne(function (err, chat) {
		      if (err) res.send(err);

			  	var livechat = {};

			    var startTime = new Date(chat.time_start);
			    var endTime = new Date(chat.time_end);

			  	livechat.expert = chat.expert;
			  	livechat.expert_name = chat.expert_name;
			  	livechat.topic = chat.topic;
			  	livechat.caption = chat.caption;
			  	livechat.date = chat.date;
			  	livechat.time = pad(startTime.getHours()) + ' - ' + getMid(pad(endTime.getHours()));
			  	livechat.time_end = chat.time_end;

			  	contentChat.main = livechat;

	        callback();
		    });
	    },
	    function (callback) {

	     	UserOnline.find(function (err, users) {
	        if (err) res.send(err);

	        var userList = [];

	        users.forEach(function (item) {
	          if (item.uid != contentChat.main.expert) {
	            userList.push({uid: item.uid, full_name: item.full_name});
	          }
	        });

	        contentChat.userList = userList;

	        callback();
	      });
	    },
	  ], function (err) {
	    if (err) return next(err);

	    return res.send(contentChat);
	  });

  });

  function chatContent(req, res, next) {
  	var contentChat = {};

    contentChat.userMessage = [];

    contentChat.AnswerTemplate = [];

	  Async.series([
	    function (callback) {

		    Livechat.findOne(function (err, chat) {
		      if (err) res.send(err);

			  	var livechat = {};

			    var startTime = new Date(chat.time_start);
			    var endTime = new Date(chat.time_end);

			  	livechat.expert = chat.expert;
			  	livechat.expert_name = chat.expert_name;
			  	livechat.topic = chat.topic;
			  	livechat.date = chat.date;
			  	livechat.time = pad(startTime.getHours()) + ' - ' + getMid(pad(endTime.getHours()));
			  	livechat.time_end = chat.time_end;
			  	livechat.user = {
			  		id: req.user._id,
			  		full_name: req.user.full_name,
			  		avatar: req.user.avatar
			  	};

			  	console.log(req.user);

			  	contentChat.main = livechat;

		    	if (req.params.id) {

			    	if (chat.expert == req.user._id) {
				      chat.active_chat = req.params.id;

				      chat.save(function (err, chatUpdated) {
				      });
				    }
		    	}

	        callback();
		    });
	    },
	    function (callback) {

	     	UserOnline.find(function (err, users) {
	        if (err) res.send(err);

	        var userList = [];

	        users.forEach(function (item) {
	          if (item.uid != contentChat.main.user.id) {

	          	var unread = 0;

				    	if (req.params.id != item.uid) {
				        item.message.forEach(function (message) {
				        	if (message.unread == '1') {
				          	unread++;
				        	}
				        });
				    	}

	            userList.push({uid: item.uid, full_name: item.full_name, unread: unread});
	          }
	        });

	        contentChat.userList = userList;

	        callback();
	      });
	    },
	    function (callback) {

	    	if (req.params.id) {
					UserOnline.findOne({uid: req.params.id}, function (err, user) {  

		        if (user) {
			        contentChat.userMessage = user.message;
		        }

			    	callback();
					});
	    	} else {
		    	callback();
	    	}
	    },
	    function (callback) {
	    	if (contentChat.main.expert == req.user._id) {
					AnswerTemplate.find(function (err, answer) {

		        contentChat.AnswerTemplate = answer;

			    	callback();
		      }).sort({_id: -1});
	    	} else {
		    	callback();
	    	}
	    }
	  ], function (err) {
	    if (err) return next(err);

	    return res.send(contentChat);
	  });
  }

  app.route('/chat')
  .get(isAuthenticated, function (req, res, next) {

  	chatContent(req, res, next);
  });

  app.route('/chat/:id')
  .get(isAuthenticated, function (req, res, next) {
  	chatContent(req, res, next);
  });

  app.route('/chat/saveAsTemplate')
  .post(function (req, res) {

    var template = new AnswerTemplate();

    template.text = req.body.message;

    template.save(function (err, data) {
      if (err) {
        res.send({response: 'error'});
      } else {
        res.send({response: 'success', id: data._id});
      }
    });
	});

  app.route('/chat/removeTemplate/:id')
  .post(function (req, res) {
		AnswerTemplate.findOne({_id: req.params.id}, function (err, template) {  

      if (template) {

      	template.remove(function (err, templateDel) {
		      if (err) {
		        res.send({response: 'error'});
		      } else {
		        res.send({response: 'success'});
		      }
      	});
      }
		});
	});

	app.io.sockets.on('connection', function (socket) {

		socket.on('login', function (data) {

	    var online = new UserOnline();

			socket.uid = data.id;

     	UserOnline.findOne({uid: data.id}, function (err, user) {
        if (!user) {

		      online.uid = data.id;
		      online.full_name = data.full_name;
		      online.avatar = data.avatar;
		    	online.updated_date = new Date();

		      online.save(function (err, user) {
		        if (!err) {
							users.push(data);

							app.io.sockets.emit('get users', users);
		        }
		      });
		    } else {
		    	user.updated_date = new Date();

		      user.save(function (err, user) {
		        if (!err) {
		        }
		      });
        };
      });
		});

		socket.on('disconnect', function (data) {
			setTimeout(function () {

				var lastUpdate = new Date();
				lastUpdate.setSeconds(lastUpdate.getSeconds() - 10);

				UserOnline.findOne({uid: socket.uid, updated_date: {$lt: lastUpdate}}, function (err, user) {  

	        if (user) {

	        	user.remove(function (err, userDel) {

							users.splice(getIndexBy(users, 'uid', socket.uid), 1);

							app.io.sockets.emit('get users', users);
	        	});
	        }
				});

			}, 10000);
		});

		//Send message
		socket.on('send message', function (data) {
	    var date = new Date();

	    var message = [];
	    var active_chat = '';


		  Async.series([
		    function (callback) {

			    Livechat.findOne(function (err, chat) {
			    	active_chat = chat.active_chat;

		        callback();
			    });
		    }
		  ], function (err) {

		  	var unread = '1';
		  	if (active_chat == data.id) {
		  		unread = '0';
		  	}

		    var messageDetail = {msg: data.message,
												    	sender: data.sender,
												    	avatar: data.avatar,
												    	name: data.name,
												    	time: date.getHours() + ':' + date.getMinutes(),
												    	unread: unread
												    };

	     	UserOnline.findOne({uid: data.id}, function (err, online) {

			  	if (active_chat == data.id) {
		        online.message.forEach(function (item) {
		        	item.unread = '0';

			        message.push(item);
		        });
			  	} else {
		     		message = online.message;
			  	}

	        message.push(messageDetail);

	        online.message = message;

		      online.save(function (err, user) {

						app.io.sockets.emit('new message ' + data.id, messageDetail);

						app.io.sockets.emit('message-notif', data.sender);
		      });
	      });
		  });
		});
	});
};

function isAuthenticated (req, res, next) {

  if (req.isAuthenticated())
    return next();

	return res.send({unauthorized: true});
}
