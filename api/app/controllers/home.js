var express = require('express'),
  router = express.Router(),
  Async = require('async'),
  mongoose = require('mongoose'),
  Categories = mongoose.model('categories'),
  Articles = mongoose.model('articles'),
  Sliders = mongoose.model('slider'),
  Ellipsis = require('text-ellipsis'),
  UrlSafeString = require('url-safe-string'),
  tagGenerator  = new UrlSafeString(),
  replaceString = require('replace-string');

var baseUrl = 'http://localhost:4711';
//var baseUrl = 'http://pocarisweatsportscience.id/apipss';

module.exports = function (app) {

  app.set('json spaces', 2);

  app.use('/', router);
};

function SortByNumber(x,y) {
  return x.no - y.no;
}

router.get('/', function (req, res, next) {
  res.send('pocari-sweat-sportscience');
});

router.get('/content', function (req, res, next) {
  contentData(req, res, next);
});

router.get('/content/:type', function (req, res, next) {
  contentData(req, res, next);
});

function thumbnailContent(item) {
  if (item.image_thumbnail) {
    var thumbnail = item.image_thumbnail;
  } else {
    var thumbnail = item.image;
  }

  var data = {
    type: item.type,
    title: item.title,
    caption: item.caption,
    titleShort: Ellipsis(item.title, 45),
    image: thumbnail,
    writer: item.writer[0].name,
    columns: item.columns,
    url: tagGenerator.generate(item.title) + '/' + item.id,
    tag: item.tag,
  }

  return data;
}

function contentData(req, res, next) {
  var type = req.params.type;
  var contentCategory = [];
  var contentList = [];
  var sliderList = [];

  if (type) {

    var ContentTypeFilter = [type];

    var CtegoryTypeFilter = {status: '1', type: type};
  } else {

    var ContentTypeFilter = ['article', 'video', 'event'];

    var CtegoryTypeFilter = {status: '1'};
  }

  Async.series([
    function (callback) {

      Categories.find(CtegoryTypeFilter, function (err, category) {
        if (err) callback(err);

        var no = 0;
        category.forEach(function (item) {

          no++;

          item.no = no;

          contentCategory.push(item);
        });

        callback();
      }).sort({order_number: 1});

    }, function (callback) {
      if (type) {
        callback();
      } else {

        Sliders.find({status: 1}, function (err, slider) {
          if (err) callback(err);

          slider.forEach(function (item) {

            var data = {
              image: item.image,
              image_mobile: item.image_mobile,
              url: item.url
            }

            sliderList.push(data);
          });

          callback();

        }).sort({order_number: 1});
      }
    }
  ], function (err) {
    if (err) return next(err);

    Async.forEachOf(contentCategory, function (value, key, callback) {

      var result = {};

      Articles.find({category: value.name, status: '1', type: {$in: ContentTypeFilter}}, function (err, article) {
        if (err) callback(err);

        var articleList = [];

        article.forEach(function (item) {

          var data = thumbnailContent(item);

          articleList.push(data);
        });

        result.no = value.no;

        result.category = value.name;

        result.category_alias = value.alias;

        result.content = articleList;

        contentList.push(result);

        callback();

      }).sort({columns: 1, _id: -1});

    }, function (err) {

      contentList.sort(SortByNumber);

      return res.send({data: contentList, slider: sliderList});
    });
  });
}

router.get('/article/:id', function (req, res, next) {

  var contentDetail = {};

  Async.series([
    function (callback) {

      Articles.findOne({_id: req.params.id}, function (err, article) {
        if (err) res.send(err);

        var content = replaceString(article.content, 'src="uploads', 'src="' + baseUrl + '/uploads')

        var data = {
          header_type: article.header_type,
          category: article.category,
          title: article.title,
          caption: article.caption,
          image: article.image,
          content: content,
          writer: article.writer[0].name,
          date: publishedDate(article.date),
        }

        contentDetail.main = data;

        callback();
      });
    }, function (callback) {
      Articles.find({category: contentDetail.main.category, type: 'article', status: '1'}, function (err, article) {
        if (err) callback(err);

        var relatedList = [];

        article.forEach(function (item) {

          if (item._id != req.params.id) {
            var data = thumbnailContent(item);

            relatedList.push(data);
          }
        });

        contentDetail.related = relatedList;

        callback();

      }).sort({columns: 1, _id: -1});
    }
  ], function (err) {
    if (err) return next(err);

    return res.send(contentDetail);
  });
});


router.get('/event/:id', function (req, res, next) {

  var data = {};

  Async.series([
    function (callback) {

      Articles.findOne({_id: req.params.id}, function (err, event) {
        if (err) res.send(err);

        /*
        var detailContent = [];

        event.content.forEach(function (item) {

          if (item.body_content) {
            item.body_content = replaceString(item.body_content, 'src="uploads', 'src="' + baseUrl + '/uploads');

            item.body_content = replaceString(item.body_content, 'background-image: url(uploads/', 'background-image: url(' + baseUrl + '/uploads/');
          }

          if (item.event_date) {
            item.event_date = publishedDate(item.event_date);
          }

          detailContent.push(item);
        });

        event.content = detailContent;
        */

        data.detail = event;

        callback();
      }).sort({date: 1});
    }, function (callback) {
      var detailContent = [];

      data.detail.content.forEach(function (item) {
        if (item.status == '1') {
          if (item.body_content) {
            item.body_content = replaceString(item.body_content, 'src="uploads', 'src="' + baseUrl + '/uploads');

            item.body_content = replaceString(item.body_content, 'background-image: url(uploads/', 'background-image: url(' + baseUrl + '/uploads/');
          }

          if (item.event_date) {
            item.event_date = publishedDate(item.event_date);
          }

          if (item.type == 'c') {
            item.child_content = [];

            Articles.find({tag: item.content_tag, status: '1', type: {$in: ['video', 'article']}}, function (err, article) {
              if (err) callback(err);

              var articleList = [];

              article.forEach(function (articleItem) {

                var data = thumbnailContent(articleItem);

                articleList.push(data);
              });

              item.child_content = articleList;

            }).sort({columns: 1, _id: -1});
          }

          detailContent.push(item);
        }
      });

      data.detail.content = detailContent;

      callback();
    }, function (callback) {
      Articles.find({tag: data.detail.tag, status: '1', type: {$in: ['video', 'article']}}, function (err, article) {
        if (err) callback(err);

        var relatedList = [];

        article.forEach(function (item) {

          var data = thumbnailContent(item);

          relatedList.push(data);
        });

        data.related = relatedList;

        callback();

      }).sort({columns: 1, _id: -1});
    }
  ], function (err) {
    if (err) return next(err);

    return res.send(data);
  });
});


router.get('/campaign/:title', function (req, res, next) {
  campaignData(req, res, next);
});

router.get('/video/:id', function (req, res, next) {

  var contentDetail = {};

  Async.series([
    function (callback) {

      Articles.findOne({_id: req.params.id}, function (err, article) {
        if (err) res.send(err);

        var content = replaceString(article.content, 'src="uploads', 'src="' + baseUrl + '/uploads');

        var data = {
          category: article.category,
          video: article.caption,
          title: article.title,
          image: article.image,
          content: content,
          writer: article.writer[0].name,
          date: publishedDate(article.date),
        }

        contentDetail.main = data;

        callback();
      });
    }, function (callback) {
      Articles.find({category: contentDetail.main.category, type: 'video', status: '1'}, function (err, article) {
        if (err) callback(err);

        var relatedList = [];

        article.forEach(function (item) {

          if (item._id != req.params.id) {
            var data = thumbnailContent(item);

            relatedList.push(data);
          }
        });

        contentDetail.related = relatedList;

        callback();

      }).sort({columns: 1, _id: -1});
    }
  ], function (err) {
    if (err) return next(err);

    return res.send(contentDetail);
  });
});

router.get('/campaign/:title/:subtitle', function (req, res, next) {
  campaignData(req, res, next);
});

function campaignData(req, res, next) {
  var campaign = {};
  var titleVal = req.params.title;
  var subtitleVal = req.params.subtitle;

  if (req.params.subtitle) {
    var mainQuery = {'campaign.slug': titleVal, slug: subtitleVal, status: 1, type: 'campaign'};
  } else {
    var mainQuery = {'campaign.slug': titleVal, status: 1, type: 'campaign'};
  }

  Async.series([
    function (callback) {

      Articles.findOne(mainQuery, function (err, article) {
        if (err) res.send(err);

        article.content = replaceString(article.content, 'src="uploads', 'src="' + baseUrl + '/uploads');

        article.content = replaceString(article.content, 'background-image: url(uploads/', 'background-image: url(' + baseUrl + '/uploads/');

        campaign.detail = article;

        callback();
      }).sort({date: 1});
    },

    function (callback) {
      Articles.find({parent_id: campaign.detail._id, type: 'article'}, function (err, article) {
        if (err) callback(err);

        var articleList = [];

        article.forEach(function (item) {

          if (item._id != req.params.id) {
            var data = thumbnailContent(item);

            articleList.push(data);
          }
        });

        campaign.related = articleList;

        callback();

      }).sort({columns: 1, _id: -1});

    },
    function (callback) {
      Articles.findOne({'campaign.slug': titleVal, _id: {$lt: campaign.detail._id}, status: 1, type: 'campaign'}, function (err, article) {
        if (err) res.send(err);

        if (article != null) {
          campaign.prev = {
            title: article.title,
            url: req.params.title + '/' + tagGenerator.generate(article.title),
          };
        } else {
          campaign.prev = null;
        }

        callback();
      }).sort({date: -1});
    },
    function (callback) {
      Articles.findOne({'campaign.slug': titleVal, _id: {$gt: campaign.detail._id}, status: 1, type: 'campaign'}, function (err, article) {
        if (err) res.send(err);

        if (article != null) {
          campaign.next = {
            title: article.title,
            url: req.params.title + '/' + tagGenerator.generate(article.title),
          };
        } else {
          campaign.next = null;
        }

        callback();
      }).sort({date: 1});
    }, function (callback) {

      Articles.find({'campaign.slug': titleVal, status: 1, type: 'campaign'}, function (err, article) {
        if (err) res.send(err);

        var List = [];

        article.forEach(function (item) {

          if (item._id != req.params.id) {
            var data = {
              title: item.title,
              url: req.params.title + '/' + tagGenerator.generate(item.title),
            }

            List.push(data);
          }
        });

        campaign.all = List;

        callback();
      }).sort({created_at: 1});
    }
  ], function (err) {
    if (err) return next(err);

    return res.send(campaign);
  });
}

function publishedDate (date) {

  if (date != '') {
    var date = new Date(date);

    var monthNames = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'];

    return monthNames[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
  } else {
    return '';
  }
}
