var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  User = mongoose.model('User');
  session = require('express-session'),
  passport = require('passport'),
  expressValidator = require('express-validator'),
  multer = require('multer'),
  crypto = require('crypto'),
  mime = require('mime-types'),
  date = new Date(),
  storage = multer.diskStorage({
    destination: 'public/uploads/' + date.getFullYear() + '/' + pad(date.getMonth()) + '/',
    filename: function (req, file, callback) {
      crypto.pseudoRandomBytes(16, function (err, raw) {
        callback(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
      });
    }
  }),
  upload = multer({storage: storage}),
  cors = require('cors');

function pad(n) {
    return (n < 10) ? ("0" + n) : n;
}

module.exports = function (app) {

  app.use(expressValidator({
  }));

  app.route('/auth/registration')
  .post(function (req, res) {

    req.checkBody('email', 'Enter a valid email address.').notEmpty().isEmail();
    req.checkBody('password', 'Enter password.').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
      res.send({response: 'errorInput', message: errors});
    } else {
      var user = new User();

      user.email = req.body.email;
      user.password = req.body.password;
      user.date = new Date();

      user.save(function (err, data) {
        if (err) {
          res.send({response: 'error'});
        } else {
          res.send({response: 'success', objectId: data._id});
        }
      });
    }
  });

  app.route('/profile/avatar')
  .post(upload.single('avatar'), function (req, res) {

    User.findOne({_id: req.body.objectId}, function (err, userDetail) {

      userDetail.avatar = req.file.path.replace('public/uploads/', '');

      userDetail.save(function (err, updatedUser) {
        if (err) {
          res.send({response: err});
        } else {
          res.send({response: 'success', path: userDetail.avatar, file: req.file.path});
        }
      });
    });
  });

  app.route('/auth/userDetail')
  .post(function (req, res) {

    req.checkBody('full_name', 'Enter Full Name.').notEmpty();
    req.checkBody('gender', 'Choose Gender.').notEmpty();
    req.checkBody('address', 'Enter Address.').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
      res.send({response: 'errorInput', message: errors});
    } else {

      User.findOne({_id: req.body.objectId}, function (err, userDetail) {

        userDetail.full_name = req.body.full_name;
        userDetail.gender = req.body.gender;
        userDetail.address = req.body.address;
        //userDetail.birthday = new Date(req.body.day + '/' + req.body.month + '/' + req.body.year);

        userDetail.save(function (err, updatedUser) {
          if (err) {
            res.send({response: err});
          } else {
            res.send({response: 'success', objectId: req.body.objectId});
          }
        });
      });
    }
  });

  app.route('/auth/personalization')
  .post(function (req, res) {

    req.checkBody('sport', 'Pilihan olah raga tidak boleh kosong').notEmpty();
    req.checkBody('range', 'Pilihan waktu olah raga').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
      res.send({response: 'errorInput', message: errors});
    } else {

      User.findOne({_id: req.body.objectId}, function (err, userDetail) {

        userDetail.sport = req.body.sport;
        userDetail.range = req.body.range;

        userDetail.save(function (err, updatedUser) {
          if (err) {
            res.send({response: err});
          } else {
            res.send({response: 'success', objectId: req.body.objectId, 'sport': req.body.sport});
          }
        });
      });
    }
  });

  app.route('/userDetail/:id')
  .get(function (req, res) {
    User.findOne({_id: req.params.id}, function (err, userDetail) {

      var data = {};

      if (userDetail.birthday) {
        var birthday = new Date(userDetail.birthday);
        data.birthday = {day: birthday.getDate(), month: birthday.getMonth(), year: birthday.getFullYear()}
      } else {
        data.birthday = '';
      }

      data.avatar = userDetail.avatar;
      data.full_name = userDetail.full_name;
      data.gender = userDetail.gender;
      data.address = userDetail.address;
      data.sport = userDetail.sport;
      data.objectId = userDetail._id;

      res.send({data: data});
    });
  });
};

function isAuthenticated (req, res, next) {

  if (req.isAuthenticated())
    return next();

  return res.send({unauthorized: true});
}