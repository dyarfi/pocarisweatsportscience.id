var express = require('express'),
  passport = require('passport'),
  Async = require('async'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Navigation = mongoose.model('navigation'),
  Articles = mongoose.model('articles');

module.exports = function (app) {
  app.route('/navigation')
  .get(function (req, res, next) {

    var contentCategory = [];
    var userData = {};

    Async.series([
      function (callback) {

        Navigation.find(function (err, category) {
          if (err) callback(err);

          category.forEach(function (item) {
            contentCategory.push(item);
          });

          callback();
        }).sort({order_number: 1});
      }, function (callback) {
        var newData = [];
        contentCategory.forEach(function (category) {
          var childContent = [];
          var categoryData = {};

          if (category.type == 'event') {
            Articles.find({type: category.type}, function (err, article) {
              if (err) callback(err);

              var articleList = [];

              article.forEach(function (articleItem) {
                articleList.push(articleItem);
              });

              categoryData.child = articleList;

            }).sort({columns: 1, _id: -1});
          }

          categoryData.type = category.type;

          categoryData.name = category.name;

          categoryData.slug = category.slug;

          newData.push(categoryData);
        });

        contentCategory = newData;

        callback();
      }, function (callback) {
        if (req.isAuthenticated()) {

          User.findOne({_id: req.user.id}, function (err, userDetail) {

            var data = {};

            if (userDetail.birthday) {
              var birthday = new Date(userDetail.birthday);
              data.birthday = {day: birthday.getDate(), month: birthday.getMonth(), year: birthday.getFullYear()}
            } else {
              data.birthday = '';
            }

            data.avatar = userDetail.avatar;
            data.full_name = userDetail.full_name;
            data.email = userDetail.email;
            data.gender = userDetail.gender;
            data.address = userDetail.address;
            data.sport = userDetail.sport;
            data.range = userDetail.range;
            data.objectId = userDetail._id;

            userData = data;

            callback();
          });
        } else {
          callback();
        }
      }
    ], function (err) {
      if (err) return next(err);

      return res.send({data: contentCategory, user: userData});
    });

  });
};
